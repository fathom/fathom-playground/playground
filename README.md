# The Playground
This is a distributed learning community creating new learning infrastructure.
We're embarking on 6-week long learning adventures to create knowledge and
explore the future of learning.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [The Playground](#the-playground)
    - [Motivation and Vision](#motivation-and-vision)
- [What's a Learning Adventure?](#whats-a-learning-adventure)
    - [Step 1. Propose an Adventure](#step-1-propose-an-adventure)
        - [A Goal](#a-goal)
        - [Structures](#structures)
        - [Community](#community)
        - [Artifacts and Outcomes](#artifacts-and-outcomes)
    - [Step 2. Get your proposal merged in](#step-2-get-your-proposal-merged-in)
        - [Criteria](#criteria)
    - [Step 3. Embark on Your Adventure](#step-3-embark-on-your-adventure)
        - [Good Things To Do:](#good-things-to-do)
    - [Step 4. Get Credentials](#step-4-get-credentials)
        - [The Fathom Network](#the-fathom-network)
- [Adventure Grants](#adventure-grants)
    - [Sponsoring Adventure Grants](#sponsoring-adventure-grants)
- [Growing the Playground](#growing-the-playground)
    - [Questions](#questions)
        - [Playground Parameters and Structures](#playground-parameters-and-structures)
        - [Fathom](#fathom)
    - [Getting Data](#getting-data)

<!-- markdown-toc end -->

## Motivation and Vision
Today's learning institutions are floundering. They divide ideas and people into
arbitrary silos, creating barriers against collaboration in the pursuit of
knowledge.

The tools and systems available today allow us to do better. We can build
structures that enable individuals to organize without intermediation, working
towards the goals that matter to them.

**This is a community to explore this future**

The playground challenges you to envision your ideal learning environment build
it, and connect it to others. By enabling these connectionsm and prompting
intentional creation, we're empowering learning above all, and spawning novel
learning structures that will scale and propagate throughout society.

----------

# What's a Learning Adventure?

The Playground is composed of **learning adventures**. These are 6-week long journeys
exploring the future of learning. Each learner on an adventure defines its goal
and structure, what they're learning and how.

Anyone and everyone is welcome to embark on their own adventure. This community
is here to support you, hold you accountable, and foster connections.

Each adventure is defined with a public documents in [/adventures](/adventures).
These documents describe both the structure and execution of an adventure.

## Step 1. Planning your adventure
Crucially, an adventure document is written _before_ you embark on your
adventure. This is the point to plan out _how_ you're going to learn, and what
systems, tools, and resources you're going to use.

Here are some things you _might_ need to explain:

- What books/vidoes/people/ideas you'll be using?
- What artifacts are you going to be making? Code, Writing, Media?
- What structures or systems will you follow?

Remember, you're adventure is both for you and for the community. Writing it
specifically for the latter (perhaps in the 2nd person) is a good way to check
your assumptions and think deeply about what you're embarking on.

**Once you've figured this all out you can submit your adventure by opening a [Merge
Request](https://gitlab.com/fathom/fathom-playground/playground/merge_requests/new)
to add an adventure document.**

## Step 2. Get your proposal merged in
Once you create a proposal, it's public. Others can comment on it, fork it, or
suggest changes. You can mark your proposal as Work-In-Progress (or WIP) if you would
like for it to be public but not yet merged in.

### Criteria:
We're not here to pass value judgements. It's up to you to decide what you think
is important to explore. Instead, the community validates the following:

- Is it a learning adventure?
- Is your adventure well defined and explicit?
- Is it realistic? Are you trying to do something impossible or unreasonable?
- Is it novel/have you connected it to others who are doing something similar?
- Do you plan on doing it?

**Anyone who has completed an adventure in the last 2 months can verify your
proposal and merge it in.**

## Step 3. Embark on Your Adventure

Once your adventure actually begins, what it looks like is entirely how you
define it. This is the most exciting part.

### Good Things To Do:

- Keep a public log of your learning experience
- Talk to other people on similar adventures!
- Set sub-goals for yourself
- Find some people to hold you accountable and check in on you
- Find some newer adventurers and support them!

**You're a part of this community, talking to other adventurers, creating the
educational systems of the future.**

## Step 4. Get Credentials

Credentials are a powerful tool within The Playground. We use the
[fathom-protocol](https://fathom.network) to collaboratively create and issue
credentials in any concept or subject that we need to.

Other adventurers who have gone before you in similar fields act as assessors of
your experience, and once you're assessed you can do the same for those who come after.
These are supportive, growing credentials defined by community consensus, not
by a central authority.

**You can use these credentials both within The Playground, to shape and connect
your learning experience, and outside of it, to communicate that experience to
others.**

### The Fathom Network:

The Playground and the Fathom Network are tied together. We are the first users
of this protocol. Creating new concepts, and coming to consensus on what they
mean and how they can be assessed, is integral to leveraging credentials in your
adventures.

# Adventure Grants

Anyone can start an adventure of their own volition and using whatever resources
they have. 

To empower and support ambitious adventures, we are aiming to offer a series of
grants to adventurers. Each of these will have their own application process and
criteria behind them to encourage a diversity of directions. While The
Playground itself welcomes all adventures in any field or direction (as long as
it's structured for learning), grants can be constrained to specific domains or
types.

You can check out grants in [/grants](./grants)

## Sponsoring Adventure Grants

We're looking for individuals and insitutions to sponsor adventures. If you're
in need of better learning systems in the world (aren't we all) or want to see
specific kinds of adventures run, please reach out, either by creating an issue
in this repository or sending an email to `contact(at)fathom.network`. 

# Growing the Playground
Right now the playground is small. As we spawn and run adventures we want to
reflect, iterate, and transform the playground into a solid foundation.

This document is the source of truth for the playground community. Anyone can
propose changes to it by opening up a Merge Request. As such, we hope that it
will develop to reflect the values and processes that drive our community.

## Questions
We have some questions we want to answer to inform this process. These are
meant to guide changes to The Playground and the tools it uses.

### Questions about Playground Parameters and Structures:
- Are the constraints around adventures optimal?
- Are the tools and systems we're using to collaborate working towards our goals?
- Is our review process effective and fair, both for grants and for regular adventures?
- Is the playground process reproducible/forkable/scalable?
- What kinds of adventures are succeeding/failing?

### Questions about Fathom:
While the fathom protocol is not core to The Playground, it is a tool
we are using and building on. As such, we want to explore how it's working and
how we can improve it.
- What tools and systems are neccessary for interacting with the fathom protocol?
- What are the use-cases for fathom credentials in The Playground?
- Are we able to come to consensus on assessments?
- Are the parameters of the Fathom Network appropriate for our use, including the following?:
  - Consensus Distance
  - Minimum Size
  - Inflation Rate
  - Challenge Periods
  
## Getting Data
All adventures and discussions in this repo are public, and so are an excellent
source of data to answer these questions. However, we also want to capture more
in-depth data in the form of interviews with adventurers. These will be publicly
compiled and shared in their own repository (or could even become an adventure of
their own).
